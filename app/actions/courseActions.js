/**
 * Command that can affect the state of application
 */
'use strict';

import courseApi from '../api/mockCoursesApi';
import { browserHistory } from 'react-router';


export const CHANGE_CREATE_COURSE_MODAL_STATUS = 'CHANGE_CREATE_COURSE_MODAL_STATUS';
export const CREATE_COURSE_SUCCESS = 'CREATE_COURSE_SUCCESS';
export const LOAD_COURSES_SUCCESS =  'LOAD_COURSES_SUCCESS';
export const UPDATE_COURSE = 'UPDATE_COURSE';
export const UPLOAD_FILES = 'UPLOAD_FILES';
export const GET_COURSE_SUCCESS = 'GET_COURSE_SUCCESS';

export const ADD_CHAPTER = 'ADD_CHAPTER';
export const EDIT_CHAPTER = 'EDIT_CHAPTER';
export const DELETE_CHAPTER = 'DELETE_CHAPTER';
export const SAVE_CHAPTER = 'SAVE_CHAPTER';
export const UP_CHAPTER_ORDER = 'UP_CHAPTER_ORDER';
export const DOWN_CHAPTER_ORDER = 'DOWN_CHAPTER_ORDER';

export const ADD_CONTENT = 'ADD_CONTENT';
export const EDIT_CONTENT = 'EDIT_CONTENT';
export const DELETE_CONTENT = 'DELETE_CONTENT';
export const SAVE_CONTENT = 'SAVE_CONTENT';

export function changeCreateCourseModalStatus(modalCreateCourseOpened) {
    console.log(`CHANGE_CREATE_COURSE_MODAL_STATUS ${modalCreateCourseOpened}`);
    return { type: CHANGE_CREATE_COURSE_MODAL_STATUS, modalCreateCourseOpened };
}

export function createCourseSuccess(course) {
    console.log(`CREATE_COURSE_SUCCESS ${course}`);
    return { type: CREATE_COURSE_SUCCESS, course };
}

export function updateCourse(course) {
    console.log(`UPDATE_COURSE ${course}`);
    return { type: UPDATE_COURSE, course };
}

export function uploadFiles(files) {
    console.log(`UPLOAD_FILES ${files}`);
    return { type: UPLOAD_FILES, files };
}

export function addChapter(chapter) {
    console.log(`ADD_CHAPTER ${chapter}`);
    return { type: ADD_CHAPTER, chapter };
}

export function editChapter(chapter) {
    console.log(`EDIT_CHAPTER ${chapter}`);
    return { type: EDIT_CHAPTER, chapter };
}

export function saveChapter(chapter) {
    console.log(`SAVE_CHAPTER ${chapter}`);
    return { type: SAVE_CHAPTER, chapter };
}

export function deleteChapter(chapter) {
    console.log(`DELETE_CHAPTER ${chapter}`);
    return { type: DELETE_CHAPTER, chapter };
}

export function upChapterOrder(chapter) {
    console.log(`UP_CHAPTER_ORDER ${chapter}`);
    return { type: UP_CHAPTER_ORDER, chapter };
}

export function downChapterOrder(chapter) {
    console.log(`DOWN_CHAPTER_ORDER ${chapter}`);
    return { type: DOWN_CHAPTER_ORDER, chapter };
}

export function addContent(content) {
    console.log(`ADD_CONTENT ${content}`);
    return { type: ADD_CONTENT, content };
}

export function editContent(content) {
    console.log(`EDIT_CONTENT ${content}`);
    return { type: EDIT_CONTENT,content };
}

export function saveContent(content) {
    console.log(`SAVE_CONTENT ${content}`);
    return { type: SAVE_CONTENT,content};
}

export function deleteContent(content) {
    console.log(`DELETE_CONTENT ${content}`);
    return { type: DELETE_CONTENT, content };
}

export function loadCoursesSuccess(courses) {
    console.log(`LOAD_COURSES_SUCCESS ${courses}`);
    return { type: LOAD_COURSES_SUCCESS, courses };
}

export function saveCourseSuccess(courses) {
    console.log(`LOAD_COURSES_SUCCESS ${courses}`);
    return { type: LOAD_COURSES_SUCCESS, courses };
}

export function getCourseSuccess(course) {
    console.log(`GET_COURSE_SUCCESS ${course}`);
    return { type: GET_COURSE_SUCCESS, course };
}

export function editCourse(courseId){
    return function (dispatch) {
        courseApi.getCourse(courseId).then(course => {
            dispatch(getCourseSuccess(course));
            browserHistory.push('/course/' + course.id);
        }).catch(error => {
            throw (error);
        });
    };
}

export function loadCourses() {
    return function (dispatch) {
        courseApi.getCourses().then(courses => {
            dispatch(loadCoursesSuccess(courses));
        }).catch(error => {
            throw (error);
        });
    };
}

export function createCourse(course) {
    return function (dispatch) {
        courseApi.saveCourse(course).then(course => {
            dispatch(createCourseSuccess(course));
            browserHistory.push('/course/' + course.id);
        }).catch(error => {
            throw (error);
        });
    };
}