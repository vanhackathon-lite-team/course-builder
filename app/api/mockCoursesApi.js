import delay from './delay';
import shortid from 'shortid';


var courses = [
    {
        id: 1,
        img: 'http://www.sfu.ca/content/sfu/mechatronics/current-students/undergraduate-students/student-resources/course-list/year-1/jcr:content/main_content/textimage/image.img.1280.high.jpg',
        name: 'Thinkific Yourself',
        subtitle: 'Empower your with knowledge!!!',
        description: 'Just another <b>example</b>>',
        price: 0.00,
        duration: 120,
        chapters: [
            {
                number: 1,
                title: 'Introduction',
                contents: [
                    {
                        id: 1,
                        title: 'Introduction.mp4',
                        type: 'video/mp4',
                        url: 'http://techslides.com/demos/sample-videos/small.mp4'
                    }
                ]

            },
            {
                number: 2,
                title: 'Why Thinkific?',
                contents: [
                    {
                        id: 1,
                        title: 'Introduction.mp4',
                        type: 'video/mp4',
                        url: 'http://techslides.com/demos/sample-videos/small.mp4'
                    }
                ]
            },
            {
                number: 3,
                title: 'Summary',
                contents: [
                    {
                        id: 1,
                        title: 'Introduction.mp4',
                        type: 'video/mp4',
                        url: 'http://techslides.com/demos/sample-videos/small.mp4',
                    },
                    {
                        id: 2,
                        title: 'Example.mp4',
                        type: 'video/mp4',
                        url: 'http://techslides.com/demos/sample-videos/small.mp4',
                    }
                ]
            },
        ],
        library: {
            contents: [
                {
                    title : 'audio.mp3',
                    type  : 'audio',
                    name  : 'Podcast'
                },
                {
                    title : 'text.html',
                    type  : 'web',
                    name  : 'Web Page'
                },
                {
                    title : 'document.pdf',
                    type  : 'pdf',
                    name  : 'Tutorial.pdf'
                },
                {
                    title : 'audio.mp3',
                    type  : 'audio',
                    name  : 'example'
                },
                {
                    title : 'audio.mp3',
                    type  : 'audio',
                    name  : 'example'
                },
            ]
        }
    },
    {
        id: 2,
        img: 'https://www.onehappydog.com/wp-content/uploads/online-course-img.png',
        name: 'Vanhackaton Rocks!!!',
        subtitle: 'React + Redux + Webpack ===  Vancourver! ',
        description: 'This isn\'t just another <b>example</b>>',
        price: 1.11,
        duration: 120,
        chapters: [
            {
                number: 1,
                title: 'Chapter 1',
                contents: [{
                    id: 1,
                    title: 'Introduction.mp4',
                    type: 'video/mp4',
                    url: 'http://techslides.com/demos/sample-videos/small.mp4'
                }]

            },
            {
                number: 2,
                title: 'Chapter 2',
                contents: []
            },
            {
                number: 2,
                title: 'Chapter 3',
                contents: []
            }
        ],
        library: {
            contents: [
                {
                    title : 'audio.mp3',
                    type  : 'audio',
                    name  : 'Podcast'
                },
                {
                    title : 'text.html',
                    type  : 'web',
                    name  : 'Web Page'
                },
                {
                    title : 'document.pdf',
                    type  : 'pdf',
                    name  : 'Tutorial.pdf'
                },
                {
                    title : 'audio.mp3',
                    type  : 'audio',
                    name  : 'example'
                },
                {
                    title : 'audio.mp3',
                    type  : 'audio',
                    name  : 'example'
                },
            ]
        }
    },
    {
        id: 2,
        img: 'https://www.onehappydog.com/wp-content/uploads/online-course-img.png',
        name: 'Vanhackaton Rocks!!!',
        subtitle: 'React + Redux + Webpack ===  Vancourver! ',
        description: 'This isn\'t just another <b>example</b>>',
        price: 1.11,
        duration: 120,
        chapters: [
            {
                number: 1,
                title: 'Chapter 1',
                contents: [{
                    id: 1,
                    title: 'Introduction.mp4',
                    type: 'video/mp4',
                    url: 'http://techslides.com/demos/sample-videos/small.mp4'
                }]
            },
            {
                number: 2,
                title: 'Chapter 2',
                contents: []
            },
            {
                number: 2,
                title: 'Chapter 3',
                contents: []
            }
        ],
        library: {
            contents: [
                {
                    title : 'audio.mp3',
                    type  : 'audio',
                    name  : 'Podcast'
                },
                {
                    title : 'text.html',
                    type  : 'web',
                    name  : 'Web Page'
                },
                {
                    title : 'document.pdf',
                    type  : 'pdf',
                    name  : 'Tutorial.pdf'
                },
                {
                    title : 'audio.mp3',
                    type  : 'audio',
                    name  : 'example'
                },
                {
                    title : 'audio.mp3',
                    type  : 'audio',
                    name  : 'example'
                },
            ]
        }
    },
    {
        id: 2,
        img: 'https://www.onehappydog.com/wp-content/uploads/online-course-img.png',
        name: 'Vanhackaton Rocks!!!',
        subtitle: 'React + Redux + Webpack ===  Vancourver! ',
        description: 'This isn\'t just another <b>example</b>>',
        price: 1.11,
        duration: 120,
        chapters: [
            {
                number: 1,
                title: 'Chapter 1',
                contents: [{
                    id: 1,
                    title: 'Introduction.mp4',
                    type: 'video/mp4',
                    url: 'http://techslides.com/demos/sample-videos/small.mp4'
                }]

            },
            {
                number: 2,
                title: 'Chapter 2',
                contents: []
            },
            {
                number: 2,
                title: 'Chapter 3',
                contents: []
            }
        ],
        library: {
            contents: [
                {
                    title : 'audio.mp3',
                    type  : 'audio',
                    name  : 'Podcast'
                },
                {
                    title : 'text.html',
                    type  : 'web',
                    name  : 'Web Page'
                },
                {
                    title : 'document.pdf',
                    type  : 'pdf',
                    name  : 'Tutorial.pdf'
                },
                {
                    title : 'audio.mp3',
                    type  : 'audio',
                    name  : 'example'
                },
                {
                    title : 'audio.mp3',
                    type  : 'audio',
                    name  : 'example'
                },
            ]
        }
    },
    // {
    //     id: 2,
    //     img: 'https://www.onehappydog.com/wp-content/uploads/online-course-img.png',
    //     name: 'Vanhackaton Rocks!!!',
    //     subtitle: 'React + Redux + Webpack ===  Vancourver! ',
    //     description: 'This isn\'t just another <b>example</b>>',
    //     price: 1.11,
    //     duration: 120,
    //     chapters: [
    //         {
    //             number: 1,
    //             title: 'Chapter <b>1</b>',
    //             contents: {
    //                 id: 1,
    //                 title: 'Introduction.mp4',
    //                 type: 'video/mp4',
    //                 url: 'http://techslides.com/demos/sample-videos/small.mp4'
    //             }
    //
    //         },
    //         {
    //             number: 2,
    //             title: 'Chapter <b>2</b>',
    //             contents: []
    //         },
    //         {
    //             number: 2,
    //             title: 'Chapter <b>3</b>',
    //             contents: []
    //         }
    //     ],
    //     library: {
    //         contents: []
    //     }
    // },
    // {
    //     id: 2,
    //     img: 'https://www.onehappydog.com/wp-content/uploads/online-course-img.png',
    //     name: 'Vanhackaton Rocks!!!',
    //     subtitle: 'React + Redux + Webpack ===  Vancourver! ',
    //     description: 'This isn\'t just another <b>example</b>>',
    //     price: 1.11,
    //     duration: 120,
    //     chapters: [
    //         {
    //             number: 1,
    //             title: 'Chapter <b>1</b>',
    //             contents: {
    //                 id: 1,
    //                 title: 'Introduction.mp4',
    //                 type: 'video/mp4',
    //                 url: 'http://techslides.com/demos/sample-videos/small.mp4'
    //             }
    //
    //         },
    //         {
    //             number: 2,
    //             title: 'Chapter <b>2</b>',
    //             contents: []
    //         },
    //         {
    //             number: 2,
    //             title: 'Chapter <b>3</b>',
    //             contents: []
    //         }
    //     ],
    //     library: {
    //         contents: []
    //     }
    // },
    // {
    //     id: 2,
    //     img: 'https://www.onehappydog.com/wp-content/uploads/online-course-img.png',
    //     name: 'Vanhackaton Rocks!!!',
    //     subtitle: 'React + Redux + Webpack ===  Vancourver! ',
    //     description: 'This isn\'t just another <b>example</b>>',
    //     price: 1.11,
    //     duration: 120,
    //     chapters: [
    //         {
    //             number: 1,
    //             title: 'Chapter <b>1</b>',
    //             contents: {
    //                 id: 1,
    //                 title: 'Introduction.mp4',
    //                 type: 'video/mp4',
    //                 url: 'http://techslides.com/demos/sample-videos/small.mp4'
    //             }
    //
    //         },
    //         {
    //             number: 2,
    //             title: 'Chapter <b>2</b>',
    //             contents: []
    //         },
    //         {
    //             number: 2,
    //             title: 'Chapter <b>3</b>',
    //             contents: []
    //         }
    //     ],
    //     library: {
    //         contents: []
    //     }
    // },
    // {
    //     id: 2,
    //     img: 'https://www.onehappydog.com/wp-content/uploads/online-course-img.png',
    //     name: 'Vanhackaton Rocks!!!',
    //     subtitle: 'React + Redux + Webpack ===  Vancourver! ',
    //     description: 'This isn\'t just another <b>example</b>>',
    //     price: 1.11,
    //     duration: 120,
    //     chapters: [
    //         {
    //             number: 1,
    //             title: 'Chapter <b>1</b>',
    //             contents: {
    //                 id: 1,
    //                 title: 'Introduction.mp4',
    //                 type: 'video/mp4',
    //                 url: 'http://techslides.com/demos/sample-videos/small.mp4'
    //             }
    //
    //         },
    //         {
    //             number: 2,
    //             title: 'Chapter <b>2</b>',
    //             contents: []
    //         },
    //         {
    //             number: 2,
    //             title: 'Chapter <b>3</b>',
    //             contents: []
    //         }
    //     ],
    //     library: {
    //         contents: []
    //     }
    // },
    // {
    //     id: 2,
    //     img: 'https://www.onehappydog.com/wp-content/uploads/online-course-img.png',
    //     name: 'Vanhackaton Rocks!!!',
    //     subtitle: 'React + Redux + Webpack ===  Vancourver! ',
    //     description: 'This isn\'t just another <b>example</b>>',
    //     price: 1.11,
    //     duration: 120,
    //     chapters: [
    //         {
    //             number: 1,
    //             title: 'Chapter <b>1</b>',
    //             contents: {
    //                 id: 1,
    //                 title: 'Introduction.mp4',
    //                 type: 'video/mp4',
    //                 url: 'http://techslides.com/demos/sample-videos/small.mp4'
    //             }
    //
    //         },
    //         {
    //             number: 2,
    //             title: 'Chapter <b>2</b>',
    //             contents: []
    //         },
    //         {
    //             number: 2,
    //             title: 'Chapter <b>3</b>',
    //             contents: []
    //         }
    //     ],
    //     library: {
    //         contents: []
    //     }
    // },
    // {
    //     id: 2,
    //     img: 'https://www.onehappydog.com/wp-content/uploads/online-course-img.png',
    //     name: 'Vanhackaton Rocks!!!',
    //     subtitle: 'React + Redux + Webpack ===  Vancourver! ',
    //     description: 'This isn\'t just another <b>example</b>>',
    //     price: 1.11,
    //     duration: 120,
    //     chapters: [
    //         {
    //             number: 1,
    //             title: 'Chapter <b>1</b>',
    //             contents: {
    //                 id: 1,
    //                 title: 'Introduction.mp4',
    //                 type: 'video/mp4',
    //                 url: 'http://techslides.com/demos/sample-videos/small.mp4'
    //             }
    //
    //         },
    //         {
    //             number: 2,
    //             title: 'Chapter <b>2</b>',
    //             contents: []
    //         },
    //         {
    //             number: 2,
    //             title: 'Chapter <b>3</b>',
    //             contents: []
    //         }
    //     ],
    //     library: {
    //         contents: []
    //     }
    // },
    // {
    //     id: 2,
    //     img: 'https://www.onehappydog.com/wp-content/uploads/online-course-img.png',
    //     name: 'Vanhackaton Rocks!!!',
    //     subtitle: 'React + Redux + Webpack ===  Vancourver! ',
    //     description: 'This isn\'t just another <b>example</b>>',
    //     price: 1.11,
    //     duration: 120,
    //     chapters: [
    //         {
    //             number: 1,
    //             title: 'Chapter <b>1</b>',
    //             contents: {
    //                 id: 1,
    //                 title: 'Introduction.mp4',
    //                 type: 'video/mp4',
    //                 url: 'http://techslides.com/demos/sample-videos/small.mp4'
    //             }
    //
    //         },
    //         {
    //             number: 2,
    //             title: 'Chapter <b>2</b>',
    //             contents: []
    //         },
    //         {
    //             number: 2,
    //             title: 'Chapter <b>3</b>',
    //             contents: []
    //         }
    //     ],
    //     library: {
    //         contents: []
    //     }
    // },
    // {
    //     id: 2,
    //     img: 'https://www.onehappydog.com/wp-content/uploads/online-course-img.png',
    //     name: 'Vanhackaton Rocks!!!',
    //     subtitle: 'React + Redux + Webpack ===  Vancourver! ',
    //     description: 'This isn\'t just another <b>example</b>>',
    //     price: 1.11,
    //     duration: 120,
    //     chapters: [
    //         {
    //             number: 1,
    //             title: 'Chapter <b>1</b>',
    //             contents: {
    //                 id: 1,
    //                 title: 'Introduction.mp4',
    //                 type: 'video/mp4',
    //                 url: 'http://techslides.com/demos/sample-videos/small.mp4'
    //             }
    //
    //         },
    //         {
    //             number: 2,
    //             title: 'Chapter <b>2</b>',
    //             contents: []
    //         },
    //         {
    //             number: 2,
    //             title: 'Chapter <b>3</b>',
    //             contents: []
    //         }
    //     ],
    //     library: {
    //         contents: []
    //     }
    // },
    // {
    //     id: 2,
    //     img: 'https://www.onehappydog.com/wp-content/uploads/online-course-img.png',
    //     name: 'Vanhackaton Rocks!!!',
    //     subtitle: 'React + Redux + Webpack ===  Vancourver! ',
    //     description: 'This isn\'t just another <b>example</b>>',
    //     price: 1.11,
    //     duration: 120,
    //     chapters: [
    //         {
    //             number: 1,
    //             title: 'Chapter <b>1</b>',
    //             contents: {
    //                 id: 1,
    //                 title: 'Introduction.mp4',
    //                 type: 'video/mp4',
    //                 url: 'http://techslides.com/demos/sample-videos/small.mp4'
    //             }
    //
    //         },
    //         {
    //             number: 2,
    //             title: 'Chapter <b>2</b>',
    //             contents: []
    //         },
    //         {
    //             number: 2,
    //             title: 'Chapter <b>3</b>',
    //             contents: []
    //         }
    //     ],
    //     library: {
    //         contents: []
    //     }
    // }
];

class CoursesAPI {

    static getCourses() {
        return new Promise((resolve, reject) => {
            debugger;
            setTimeout(() => {
                resolve(Object.assign([], courses));
            }, delay)
        });
    };

    /**
     * MOCK
     * Save a course on backend and return its id if resolve the promisse
     * @param course
     * @return {Promise}
     */
    static saveCourse(course) {
        return new Promise((resolve, reject) => {
            debugger;
            setTimeout(() => {
                let newCourse = Object.assign({}, course);

                newCourse.id = shortid.generate();
                newCourse.img = `http://lorempixel.com/640/480/${Math.round(Math.random() * 2) + 1 == 1 ? 'people' : 'nature'}/${Math.round(Math.random() * 10) + 1}/`;

                courses.push(newCourse);

                resolve(newCourse);
            }, delay)
        });
    }

    static getCourse(courseId) {
        return new Promise((resolve, reject) => {
            debugger;
            setTimeout(() => {

                courses.forEach( course => {
                    if (course.id === parseInt(courseId)) {
                        return resolve(course);
                    }
                });

                reject('Not found');
            }, delay)
        });
    }
}

export default CoursesAPI;