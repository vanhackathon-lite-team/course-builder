import React from 'react';
import {Route, IndexRoute } from 'react-router';
import Dashboard from './components/Dashboard';
import Library from './components/Library';
import Courses from './components/Courses';
import EditCourse from './components/EditCourse';

export default (
    <Route path="/" component={Dashboard}>
        <IndexRoute component={Courses}/>
        <Route path="library" component={Library}/>
        <Route path="course/:id" component={EditCourse}/>
    </Route>
);