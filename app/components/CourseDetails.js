/**
 * Created by anderson on 07/04/17.
 */

import React  from 'react';
import TinyMCE from 'react-tinymce';

// Redux
import {connect} from 'react-redux';
import * as courseActions from '../actions/courseActions';
import {bindActionCreators} from 'redux';

// Material-ui
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';

const inputFieldStyle = {
    marginLeft: 10,
    width: '100%'
};

let {
    Component
} = React;

class CourseDetails extends Component {
    constructor(props, context) {
        super(props, context);

        this.onNameChange = this.onNameChange.bind(this);
        this.onSubtitleChange = this.onSubtitleChange.bind(this);
        this.onPriceChange = this.onPriceChange.bind(this);
        this.onDurationChange = this.onDurationChange.bind(this);
        this.onDescriptionChange = this.onDescriptionChange.bind(this);
    }

    onNameChange(event) {
        let courseClone = Object.assign({},this.props.courseSelected);
        courseClone.name = event.target.value;
        this.props.actions.updateCourse(courseClone);
    };

    onSubtitleChange(event) {
        let courseClone = Object.assign({},this.props.courseSelected);
        courseClone.subtitle = event.target.value;
        this.props.actions.updateCourse(courseClone);
    };

    onPriceChange(event) {
        let courseClone = Object.assign({},this.props.courseSelected);
        courseClone.price = event.target.value;
        this.props.actions.updateCourse(courseClone);
    };

    onDurationChange(event) {
        let courseClone = Object.assign({},this.props.courseSelected);
        courseClone.duration = event.target.value;
        this.props.actions.updateCourse(courseClone);
    };

    onDescriptionChange(event) {
        debugger;
        let courseClone = Object.assign({},this.props.courseSelected);
        courseClone.description = event.target.getContent({format:'raw'});
        this.props.actions.updateCourse(courseClone);
    }

    render() {
        return (
            <div>
                <TextField
                    hintText="Name"
                    floatingLabelText="Name:"
                    floatingLabelFixed={true}
                    style={inputFieldStyle}
                    name="fname"
                    size="255"
                    onChange={this.onNameChange}
                    value={this.props.courseSelected.name}
                /><br/>
                <TextField
                    hintText="Subtitle"
                    floatingLabelText="Subtitle:"
                    floatingLabelFixed={true}
                    style={inputFieldStyle}
                    name="fsubtitle"
                    size="255"
                    onChange={this.onSubtitleChange}
                    value={this.props.courseSelected.subtitle}
                /><br/>
                <TextField
                    hintText="Price"
                    floatingLabelText="Price:"
                    floatingLabelFixed={true}
                    style={{marginLeft: 10}}
                    type="number" min="0" max="99999999" maxLength="8" name="fprice" step="0.01"
                    onChange={this.onPriceChange}
                    value={this.props.courseSelected.price}
                />
                {'  |  '}
                <TextField
                    hintText="Duration"
                    floatingLabelText="Duration:"
                    floatingLabelFixed={true}
                    type="number" min="0" max="99999" maxLength="5" name="fprice" step="0.1"
                    onChange={this.onDurationChange}
                    value={this.props.courseSelected.duration}
                /><br/>
                <p>Description:</p>
                <TinyMCE
                    style={{marginLeft: 10}}
                    content={this.props.courseSelected.description}
                    config={{
                        plugins: 'link image code',
                        menubar: false,
                        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                    }}
                    onChange={this.onDescriptionChange}
                />
            </div>
        );
    }
}

CourseDetails.propTypes = {
    courseSelected: React.PropTypes.object.isRequired,
    actions: React.PropTypes.object.isRequired
};

/*
 * Provides store props to state
 */
function mapProps(state, ownProps) {
    return {
        courseSelected: state.coursesState.courseSelected,
    };
}

/*
 * Provides actions to Component
 * */
function mapActions(dispatch) {
    return {
        actions: bindActionCreators(courseActions, dispatch)
    }
}


export default connect(mapProps, mapActions)(CourseDetails);