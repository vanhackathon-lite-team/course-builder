
import React from 'react';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import TextField from 'material-ui/TextField';
import ContentAdd from 'material-ui/svg-icons/content/add';



import {connect} from 'react-redux';
import * as courseActions from '../actions/courseActions';
import {bindActionCreators} from 'redux';

const plusButtonStyle = {
    textAlign: 'justify',
    textAlignLast: 'center'
};


class AddCourse extends React.Component {
    constructor(props, context){
        super(props, context);
        this.state = {
            modalCreateCourseOpened : false,
            course : {
                id : 0,
                name : '',
                subtitle : '',
                description : '',
                price : 0.00,
                duration : 0,
                chapters : [ ],
                img : '',
                library : {
                    contents : []
                }
            },
            confirmButtonEnabled : false,
        };

        this.handleOpen = this.handleOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.onConfirm = this.onConfirm.bind(this);
        this.onNameChange = this.onNameChange.bind(this);
    }

    onNameChange(event) {
        const course = this.state.course;
        course.name = event.target.value;
        this.setState({ course : course, confirmButtonEnabled : (course.name.length > 0)});
    };

    handleOpen () {
        const course = this.state.course;
        course.name = '';
        this.setState({ course : course, confirmButtonEnabled : (course.name.length > 0)});
        this.props.actions.changeCreateCourseModalStatus(true);
    };

    handleClose () {
        this.props.actions.changeCreateCourseModalStatus(false);
    };

    onConfirm () {
        this.props.actions.createCourse(this.state.course);
    };

    render() {
        const actions = [
            <FlatButton
                label="Create"
                primary={true}
                disabled={!this.state.confirmButtonEnabled}
                keyboardFocused={true}
                onTouchTap={this.onConfirm}
            />,
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.handleClose}
            />,
        ];

        return (
            <div>
                <FloatingActionButton style={plusButtonStyle}>
                    <ContentAdd onTouchTap={this.handleOpen} />
                </FloatingActionButton>

                <Dialog
                    title="New Course"
                    actions={actions}
                    modal={false}
                    open={this.props.modalCreateCourseOpened}
                    onRequestClose={this.handleClose}
                >
                    <TextField hintText="Name" onChange={this.onNameChange} value={this.state.course.name} fullWidth={true} size="255"/>
                </Dialog>
            </div>
        );
    }
}

AddCourse.propTypes = {
    modalCreateCourseOpened: React.PropTypes.bool.isRequired,
    actions: React.PropTypes.object.isRequired
};


/*
 * Provides store props to state
 */
function mapProps(state, ownProps) {
    return {
        modalCreateCourseOpened: state.coursesState.modalCreateCourseOpened
    };
}

/*
 * Provides actions to Component
 * */
function mapActions(dispatch) {
    return {
        actions: bindActionCreators(courseActions, dispatch)
    }
}


export default connect(mapProps, mapActions)(AddCourse);