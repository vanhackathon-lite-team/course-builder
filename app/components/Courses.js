import React from "react";

import {connect} from "react-redux";
import * as courseActions from "../actions/courseActions";
import {bindActionCreators} from "redux";

import {GridList, GridTile} from "material-ui/GridList";
import IconButton from "material-ui/IconButton";
import StarBorder from "material-ui/svg-icons/toggle/star-border";
import AddCourseButton from "./AddCourse";


const styles = {
    root: {
        position: 'relative',
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
    },
    gridList: {
        width: '100%',
        height: '100%',
        overflowY: 'auto',
    },
    navDrawerDiv: {
        position: 'fixed',
        right: 0,
        bottom: 0,
    },
};


class Courses extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.editCourse = this.editCourse.bind(this);
    }

    editCourse(e) {
        debugger;
        const courseId = e.target.id;
        this.props.actions.editCourse(courseId);
    }

    render() {
        return (
            <div>
                <div className="divisoria"></div>
                <div id="divCourseContent" style={styles.root}>
                    <div id="navDrawer" style={styles.navDrawerDiv}>
                        <AddCourseButton  />
                    </div>
                    <GridList
                        cellHeight={280}
                        style={styles.gridList}
                        cols={4}
                        padding={20}
                    >
                        {this.props.courses.map((course, index) => (
                            <GridTile
                                id={course.id}
                                className="gridItem"
                                key={index}
                                title={course.name}
                                onClick={this.editCourse}
                                subtitle={<span>by <b>Me</b></span>}
                                actionIcon={<IconButton><StarBorder color="white"/></IconButton>}
                            >
                                <img src={course.img} id={course.id}/>
                            </GridTile>
                        ))}

                    </GridList>

                </div>

            </div>
        );
    }
}

Courses.propTypes = {
    courses: React.PropTypes.array.isRequired,
    actions: React.PropTypes.object.isRequired
};


/*
 * Provides store props to state
 */
function mapProps(state, ownProps) {
    debugger;
    return {
        courses: state.coursesState.courses,
    };
}

/*
 * Provides actions to Component
 * */
function mapActions(dispatch) {
    return {
        actions: bindActionCreators(courseActions, dispatch)
    }
}


export default connect(mapProps, mapActions)(Courses);