/**
 * Created by anderson on 07/04/17.
 */

import React, {PropTypes} from 'react';
import Header from './Header';
import Courses from './Courses';
import Library from './Library';

const Dashboard = (props) => {
    return (
        <div className="container-fluid">
            <Header/>
            {props.children}
        </div>
    );
};

Dashboard.propTypes = {
    children: PropTypes.object.isRequired
};

export default Dashboard;