import React from 'react';
import DropzoneComponent from 'react-dropzone-component';
var eventHandlers = {addedfile: (file) => console.log('Added: ' + JSON.stringify(file))};


var componentConfig = {
    iconFiletypes: ['.jpg', '.png', '.gif', '.pdf', '.mp4', '.mp3', '.html'],
    showFiletypeIcon: true,
    postUrl: 'upload'
};

var djsConfig = {
    autoProcessQueue: true,
    addRemoveLinks: true,
    clickable: true,
    dictDefaultMessage: 'Drag and Drop your content here!',
    acceptedFiles: "image/*, video/*, application/pdf, audio/*, text/html",
    uploadMultiple: true,
};

let imgUrl = 'img/dropzone.svg'; 

class UploadContent extends React.Component {

    render() {
        return (
            <div className="titulo">
                Dropzone
                <DropzoneComponent config={componentConfig}
                                   eventHandlers={eventHandlers}
                                   djsConfig={djsConfig}
                                   


                />,
            </div>
        );
    }

}

export default UploadContent;