/**
 * Created by anderson on 07/04/17.
 */

var React = require('react');

import Accordion from 'react-responsive-accordion';
// Redux
import {connect} from 'react-redux';
import * as courseActions from '../actions/courseActions';
import {bindActionCreators} from 'redux';

//FontIcon
import FontIcon from 'material-ui/FontIcon';
import { white } from 'material-ui/styles/colors';


let {
    Component
} = React;

let titleStyle = {
    margin : '10px',
    with :'100%',
    backgroudColor : 'gray'
};

const iconStyles = {
    marginRight: 24,
};

class CourseOutline extends Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div>
                <h2>Library</h2>
                {this.props.courseSelected.library.contents.map((content, index) => (
                    <div class="content"><strong>Content:</strong> {content.title} <strong>Type: {content.type}</strong>  <FontIcon className="material-icons" style={iconStyles}>{content.icon}</FontIcon></div>
                ))}

                <Accordion>
                    {this.props.courseSelected.chapters.map((chapter, index) => (
                        <div data-trigger={chapter.title}  style={titleStyle}  className="chapter">
                            {chapter.contents.map(content=>
                                <div class="content"><strong>Content:</strong> {content.title} <strong>Type: {content.type}</strong>
                                    <FontIcon className="material-icons" style={iconStyles}>{content.icon}</FontIcon>
                                </div>
                            )}
                        </div>
                    ))}
                </Accordion>
            </div>
        );
    }
}

CourseOutline.propTypes = {
    courseSelected: React.PropTypes.object.isRequired,
    actions: React.PropTypes.object.isRequired
};

/*
 * Provides store props to state
 */
function mapProps(state, ownProps) {
    return {
        courseSelected: state.coursesState.courseSelected,
    };
}

/*
 * Provides actions to Component
 * */
function mapActions(dispatch) {
    return {
        actions: bindActionCreators(courseActions, dispatch)
    }
}


export default connect(mapProps, mapActions)(CourseOutline);