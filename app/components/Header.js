import React from 'react';
import {Link, IndexLink} from 'react-router';
import FontIcon from 'material-ui/FontIcon';
import { white } from 'material-ui/styles/colors';

import AppBar from 'material-ui/AppBar';


const iconStyles = {
  marginRight: 36,
  marginTop: 20,
  fontSize : 36
};

const navStyle = {
  marginRight: 24,
  fontSize : 20
};

const Header = (props) => {
    return (
        <AppBar title="" iconClassNameRight="muidocs-icon-navigation-expand-more" className="header">
            <div className="logo"><img src="img/thinkific-logo.png"/></div>
            <nav>
                <IndexLink to="/" activeClassName="active" style={navStyle}>My Courses</IndexLink>

                <Link to="/library" activeClassName="active" style={navStyle}>Content Library</Link>
            </nav>
            <img src="img/search.svg" />
        </AppBar>
    );
};

export default Header;