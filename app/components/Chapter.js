import React from 'react';

let {
    Component
} = React;


class Chapter extends Component {
    render() {
        return (
            <div>
                {this.props.title}
            </div>
        );
    }
}

export default Chapter;