import React from 'react';

// Redux
import {connect} from 'react-redux';
import * as courseActions from '../actions/courseActions';
import {bindActionCreators} from 'redux';

import {
    Step,
    Stepper,
    StepButton,
} from 'material-ui/Stepper';

let {
    PropTypes,
    Component
} = React;

import CourseDetails from './CourseDetails';
import UploadContent from './UploadContent';
import CourseOutline from './CourseOutline';

const COURSE_DETAILS = 'Course Details', UPLOAD_CONTENT = '2. Content Upload', COURSE_OUTLINE = '3. Course Outline';


class CourseEdition extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            stepIndex: 0,
        };
    }

    getStepContent(stepIndex) {
        switch (stepIndex) {
            case 0:
                return <CourseDetails />;
            case 1:
                return <UploadContent />;
            case 2:
                return <CourseOutline />;
            default:
                return 'You\'re a long way from home sonny!';
        }
    }

    render() {

        const {stepIndex} = this.state;

        return (
            <div style={{width: '100%', maxWidth: 700, margin: 'auto'}}>
                <h2>{this.props.courseSelected.name}</h2>
                <Stepper linear={false} activeStep={stepIndex}>
                    <Step>
                        <StepButton onClick={() => this.setState({stepIndex: 0})}>
                            {COURSE_DETAILS}
                        </StepButton>
                    </Step>
                    <Step>
                        <StepButton onClick={() => this.setState({stepIndex: 1})}>
                            {UPLOAD_CONTENT}
                        </StepButton>
                    </Step>
                    <Step>
                        <StepButton onClick={() => this.setState({stepIndex: 2})}>
                            {COURSE_OUTLINE}
                        </StepButton>
                    </Step>
                </Stepper>
                {this.getStepContent(stepIndex)}
            </div>
        );
    }
}

CourseEdition.propTypes = {
    courseSelected: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired
};

/*
 * Provides store props to state
 */
function mapProps(state, ownProps) {
    debugger;
    return {
        courseSelected: state.coursesState.courseSelected,
    };
}

/*
 * Provides actions to Component
 * */
function mapActions(dispatch) {
    return {
        actions: bindActionCreators(courseActions, dispatch)
    }
}


export default connect(mapProps, mapActions)(CourseEdition);