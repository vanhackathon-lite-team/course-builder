import {combineReducers} from 'redux';
import coursesState from './courseReducer';

const dashboardReducer = combineReducers({ coursesState });

export default dashboardReducer;