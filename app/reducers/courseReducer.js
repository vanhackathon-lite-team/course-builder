import {
    UPDATE_COURSE,
    CREATE_COURSE_SUCCESS,
    LOAD_COURSES_SUCCESS,
    CHANGE_CREATE_COURSE_MODAL_STATUS,
    GET_COURSE_SUCCESS
} from '../actions/courseActions';

let initialState = {
    courses: [],
    modalCreateCourseOpened: false,
};

export default function courseReducer(state = initialState, action) {

    var newState = Object.assign({}, state);
    debugger;
    switch (action.type) {
        case CREATE_COURSE_SUCCESS:
            newState.courses = [...state.courses, Object.assign({}, action.course)];
            newState.modalCreateCourseOpened = false;
            newState.courseSelected = action.course;
            return newState;
        case LOAD_COURSES_SUCCESS:
            newState.courses = action.courses;
            return newState;
        case CHANGE_CREATE_COURSE_MODAL_STATUS:
            newState.modalCreateCourseOpened = action.modalCreateCourseOpened;
            return newState;
        case GET_COURSE_SUCCESS:
            newState.courseSelected = action.course;
            return newState;
        case UPDATE_COURSE:
            newState.courseSelected = action.course;
            return newState;
        default:
            return state;
    }
}
