/**
 * Created by anderson on 08/04/17.
 */


import {createStore, applyMiddleware} from 'redux';
import dashboardReducer from '../reducers/dashboardReducer';
import thunk from 'redux-thunk';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';


export default function configureStore(initialState){
    return createStore(dashboardReducer, initialState, applyMiddleware(thunk, reduxImmutableStateInvariant()));
};