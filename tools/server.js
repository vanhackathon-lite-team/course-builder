const express = require('express');
const path = require('path');
const open = require('open');
const config =  require('../webpack.config');
const fileUpload = require('express-fileupload');
const app = express();

// Serve static assets
app.use(express.static(path.resolve(__dirname, '..', config.output.path )));

// Always return the main index.html, so react-router render the route in the client
app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '..', 'dist', 'index.html'));
});

app.post('/course/upload', function(req, res) {
    res.send('File Uploaded');
});

let port = 3000;

app.listen(port);

open(`http://localhost:${port}`);