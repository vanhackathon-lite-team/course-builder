/*
 * Webpack configuration file. Specifies source files and distribution folder
 * */
var config = {
    context: __dirname + "/app",
    entry: "./App.js",

    target: 'web',

    output: {
        filename: "bundle.js",
        path: __dirname + "/dist"
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015']
                }
            },
            {test: /\.css$/, loader: "style-loader!css-loader"},
            { test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000' }
        ]
    }
};
module.exports = config;